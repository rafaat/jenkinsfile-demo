import org.junit.Assert;
import org.junit.Test;

public class DemoTests {

    @Test
    public void test1() {
        Assert.assertEquals(2, 1+1);
    }

    @Test
    public void test2() {
        Assert.assertEquals(4, 2*2);
    }

    @Test
    public void test3() {
        Assert.assertEquals(2, 2+2);
    }
}
